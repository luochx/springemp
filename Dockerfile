FROM reg.csphere.cn/java:7-jre
WORKDIR /usr/local/tomcat/webapps/
ADD target/springemp.war /usr/local/tomcat/webapps/
ADD mysql.sql /tmp/springemp/
ADD init.sh /tmp/springemp/

