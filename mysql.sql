drop database if exists emp;

create database emp;
use emp;

create table user (
	id int primary key auto_increment,
	username varchar(50) not null,
	password varchar(255) not null
);
insert into user(username, password) values('admin', '123456');

create table emp (
	id int primary key auto_increment,
	name varchar(50) not null,
	salary double ,
	age int
);
