# pull image from registry
echo '============== PULL image from registry ==============='

docker pull ${Registry_URL}/${image_repository}:${image_tag}  
docker tag -f ${Registry_URL}/${image_repository}:${image_tag} ${image_repository}:latest

# clean environment
echo '============== CLEAN environment ==============='

#停止容器
docker-compose stop

#删除容器
docker-compose rm -f

#一键部署ALL环境
docker-compose up -d


echo '============== waiting for test  ... ==============='
echo 'sleep 20 second ... ' 
sleep 20
    
HTTP_CODE=`curl -o /dev/null -s -w "%{http_code}" "${test_curl}"`
    if [ ${HTTP_CODE} -ne 200 ];
        then
        	echo "${test_curl} access failed and http_code = ${HTTP_CODE}"
    		exit 1
        else 
            echo "${test_curl} access success !"
	fi

# clean environment
echo '============== CLEAN environment ==============='
#停止容器
docker-compose stop

#删除容器
docker-compose rm -f


