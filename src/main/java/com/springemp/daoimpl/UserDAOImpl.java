package com.springemp.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.springemp.dao.UserDAO;
import com.springemp.model.User;

@Repository
public class UserDAOImpl implements UserDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public User findByName(String username) {
		System.out.println("username in UserDAOImpl = "+ username);
		String sql = "select * from user where username = ?";
		System.out.println(jdbcTemplate);
		return (User)jdbcTemplate.queryForObject(sql, new Object[]{username},new int[]{java.sql.Types.VARCHAR},new UserRowMapper());
	}

}
