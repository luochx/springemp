package com.springemp.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springemp.model.Emp;

public class EmpRowMapper implements RowMapper<Emp> {

	@Override
	public Emp mapRow(ResultSet rs, int index) throws SQLException {
		Emp emp = new Emp();
		emp.setId(rs.getInt("id"));
		emp.setName(rs.getString("name"));
		emp.setSalary(rs.getLong("salary"));
		emp.setAge(rs.getInt("age"));
		return emp;
	}

	

}
