package com.springemp.daoimpl;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.springemp.dao.EmpDAO;
import com.springemp.model.Emp;

@Transactional
@Repository
public class EmpDAOImpl implements EmpDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Emp> getEmps() { 
		String sql = "select * from emp";
		List<Emp> list = (List<Emp>)jdbcTemplate.query(sql, new EmpRowMapper());
		//return (List<Emp>)jdbcTemplate.query(sql, new EmpRowMapper());
		return list;
	}
	

	@Override
	public void deleteEmp(int id) {
		String sql = "delete from emp where id = ?";
		jdbcTemplate.update(sql,new Object[]{id},new int[]{java.sql.Types.INTEGER});
	}


	@Override
	public void addEmp(String name, long salary, int age) {
		String sql = "insert into emp(name,salary,age) values(?,?,?)";
		jdbcTemplate.update(sql,new Object[]{name,salary,age},new int[]{java.sql.Types.VARCHAR,java.sql.Types.BIGINT,java.sql.Types.INTEGER});
	}


	@Override
	public Emp getEmp(int id) {
		String sql = "select * from emp where id = ?";
		return (Emp)jdbcTemplate.queryForObject(sql, new Object[]{id}, 
				new int[]{java.sql.Types.INTEGER}, new EmpRowMapper() );
	}


	@Override
	public void updateEmp(int id, String name, long salary, int age) {
		String sql = "update emp set name = ? ,salary = ? and age = ? where id = ?";
		jdbcTemplate.update(sql, new Object[]{name,salary,age,id},new int[]{Types.VARCHAR,Types.BIGINT,Types.INTEGER,Types.INTEGER});
	}


}
