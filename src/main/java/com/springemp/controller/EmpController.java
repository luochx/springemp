package com.springemp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.springemp.dao.EmpDAO;
import com.springemp.service.UserService;

@Controller
public class EmpController {
	@Autowired
	private UserService userService;
	@Autowired
	private EmpDAO empDAO;
	
	@RequestMapping("delete")
	public ModelAndView delete(int id){
		empDAO.deleteEmp(id);
		ModelAndView mv = new ModelAndView("emplist","emps",empDAO.getEmps());
		return mv;
	}
	
	@RequestMapping("update")
	public ModelAndView update(int id){
		ModelAndView mv = new ModelAndView("updateEmp","emp",empDAO.getEmp(id));
		return mv;
	}
	
	@RequestMapping("doUpdate")
	public ModelAndView doUpdate(int id,String name,long salary,int age) {
		empDAO.updateEmp(id, name, salary, age);
		ModelAndView mv = new ModelAndView("emplist","emps",empDAO.getEmps());
		return mv;
	}
	
	@RequestMapping("add")
	public ModelAndView add(){
		ModelAndView mv = new ModelAndView("addEmp");
		return mv;
	}
	
	@RequestMapping("doAdd")
	public ModelAndView doAdd(String name,Long salary,int age) {
		empDAO.addEmp(name, salary, age);
		ModelAndView mv = new ModelAndView("emplist","emps",empDAO.getEmps());
		return mv;
	}
}
