package com.springemp.dao;

import java.util.List;

import com.springemp.model.Emp;

public interface EmpDAO {
	public List<Emp> getEmps();
	
	public Emp getEmp(int id);
	
	public void deleteEmp(int id);
	
	public void updateEmp(int id,String name,long salary,int age);
	
	public void addEmp(String name,long salary,int age);
	
}
