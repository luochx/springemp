package com.springemp.dao;

import com.springemp.model.User;

public interface UserDAO {
	public User findByName(String username);
}
