package com.springemp.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springemp.dao.UserDAO;
import com.springemp.model.User;
import com.springemp.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDAO userDAO;

	@Override
	public boolean login(String username, String password) {
		System.out.println("username in UserServiceImpl = "+username);
		User user = userDAO.findByName(username);
		if(user == null) {
			return false;
		}
		if(password.equals(user.getPassword())){
			return true;
		}
		return false;
	}

}
