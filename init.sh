#!/bin/bash
set -x

if [ ! -e /.inited ]; then
  sleep 6
  export PATH=$PATH:/usr/local/mysql/bin
  mysql -h $MYSQL_PORT_3306_TCP_ADDR -u root -proot < /tmp/springemp/mysql.sql
  touch /.inited
fi

exec $@
